package com.example.airportgatemanagementapi.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.With;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import java.sql.Time;
import java.util.UUID;

import static javax.persistence.CascadeType.ALL;

@Entity
@Data
@With
@NoArgsConstructor
@AllArgsConstructor
public class Gate {

    @Id
    private UUID id;
    private String gateNumber;
    private Time availableFrom;
    private Time availableTo;

    @OneToOne(cascade = ALL)
    @JoinColumn(name = "flight_id", referencedColumnName = "id")
    private Flight flight;
}
