package com.example.airportgatemanagementapi.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.UUID;

@Entity
@Data
@NoArgsConstructor
public class Flight {

    @Id
    private UUID id;
    private String flightNumber;
}
