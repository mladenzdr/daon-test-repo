package com.example.airportgatemanagementapi.repository;

import com.example.airportgatemanagementapi.entity.Gate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface GateRepository extends JpaRepository<Gate, UUID> {

    @Query(value = "select * " +
            "from gate " +
            "where flight_id is null" +
            "  and (available_from is null or available_from < current_time)" +
            "  and (available_to is null or available_to > current_time) " +
            "for update",
            nativeQuery = true)
    List<Gate> findAvailableGates();

    Optional<Gate> findByGateNumber(String gateNumber);
}
