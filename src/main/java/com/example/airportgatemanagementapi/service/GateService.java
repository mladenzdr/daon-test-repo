package com.example.airportgatemanagementapi.service;

import com.example.airportgatemanagementapi.service.dao.FlightDao;
import com.example.airportgatemanagementapi.service.dao.GateDao;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Time;

@Service
@RequiredArgsConstructor
public class GateService {

    private final GateDao gateDao;
    private final FlightDao flightDao;


    @Transactional
    public String getAvailableGateNumber(String flightNumber) {

        var gateOptional = gateDao.getAvailableGates().stream().findAny();

        if (gateOptional.isEmpty()) return "No available gates at the moment";

        var gate = gateOptional.get();
        gateDao.saveGate(gate.withFlight(flightDao.getFlightByFlightNumber(flightNumber)));

        return gate.getGateNumber();
    }

    @Transactional
    public void releaseGate(String gateNumber) {

        gateDao.releaseGate(gateNumber);
    }

    @Transactional
    public void updateAvailability(String gateNumber, Time availableFrom, Time availableTo) {

        gateDao.updateGateAvailability(gateNumber, availableFrom, availableTo);
    }
}
