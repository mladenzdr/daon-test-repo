package com.example.airportgatemanagementapi.service.dao;

import com.example.airportgatemanagementapi.entity.Flight;
import com.example.airportgatemanagementapi.repository.FlightRepository;
import com.example.airportgatemanagementapi.service.exception.FlightNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class FlightDao {

    private final FlightRepository flightRepository;


    public Flight getFlightByFlightNumber(String flightNumber) {
        return flightRepository.findByFlightNumber(flightNumber)
                .orElseThrow(() -> new FlightNotFoundException(flightNumber));
    }
}
