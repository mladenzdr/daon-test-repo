package com.example.airportgatemanagementapi.service.dao;

import com.example.airportgatemanagementapi.entity.Gate;
import com.example.airportgatemanagementapi.repository.GateRepository;
import com.example.airportgatemanagementapi.service.exception.GateNotFoundException;
import com.example.airportgatemanagementapi.service.exception.GateNotInUseException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.sql.Time;
import java.util.List;
import java.util.Optional;

import static java.util.Objects.nonNull;

@Component
@RequiredArgsConstructor
public class GateDao {

    private final GateRepository gateRepository;


    public void saveGate(Gate gate) {

        gateRepository.save(gate);
    }

    public List<Gate> getAvailableGates() {

        return gateRepository.findAvailableGates();
    }

    public void releaseGate(String gateNumber) {

        gateRepository.save(Optional.of(gateRepository.findByGateNumber(gateNumber)
                .orElseThrow(() -> new GateNotFoundException(gateNumber)))
                .filter(gate -> nonNull(gate.getFlight()))
                .orElseThrow(() -> new GateNotInUseException(gateNumber))
                .withFlight(null)
        );
    }

    public void updateGateAvailability(String gateNumber, Time availableFrom, Time availableTo) {

        gateRepository.save(gateRepository.findByGateNumber(gateNumber)
                .orElseThrow(() -> new GateNotFoundException(gateNumber))
                .withAvailableTo(availableTo)
                .withAvailableFrom(availableFrom)
        );
    }
}
