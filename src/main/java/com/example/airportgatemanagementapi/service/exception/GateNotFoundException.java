package com.example.airportgatemanagementapi.service.exception;

public class GateNotFoundException extends RuntimeException {

    private final String gateNumber;

    public GateNotFoundException(String gateNumber) {
        this.gateNumber = gateNumber;
    }

    @Override
    public String getMessage() {
        return "Gate with gate number: " + gateNumber + " is not found";
    }
}
