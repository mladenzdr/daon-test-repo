package com.example.airportgatemanagementapi.service.exception;

public class FlightNotFoundException extends RuntimeException {

    private final String flightNumber;

    public FlightNotFoundException(String flightNumber) {
        this.flightNumber = flightNumber;
    }

    @Override
    public String getMessage() {
        return "Flight with flight number: " + flightNumber + " is not found!";
    }
}
