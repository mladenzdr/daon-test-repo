package com.example.airportgatemanagementapi.service.exception;

public class GateNotInUseException extends RuntimeException {

    private final String gateNumber;

    public GateNotInUseException(String gateNumber) {
        this.gateNumber = gateNumber;
    }

    @Override
    public String getMessage() {
        return "Gate with gate number: " + gateNumber + " is not in use";
    }
}
