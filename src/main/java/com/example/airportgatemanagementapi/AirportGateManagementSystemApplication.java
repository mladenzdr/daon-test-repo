package com.example.airportgatemanagementapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AirportGateManagementSystemApplication {

    public static void main(String[] args) {
        SpringApplication.run(AirportGateManagementSystemApplication.class, args);
    }
}
