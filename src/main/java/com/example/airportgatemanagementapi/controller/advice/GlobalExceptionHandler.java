package com.example.airportgatemanagementapi.controller.advice;

import com.example.airportgatemanagementapi.service.exception.FlightNotFoundException;
import com.example.airportgatemanagementapi.service.exception.GateNotFoundException;
import com.example.airportgatemanagementapi.service.exception.GateNotInUseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import static java.util.stream.Collectors.toMap;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.http.HttpStatus.NOT_FOUND;

@ControllerAdvice
@ResponseBody
public class GlobalExceptionHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(GlobalExceptionHandler.class);

    @ExceptionHandler({GateNotFoundException.class, FlightNotFoundException.class})
    @ResponseStatus(NOT_FOUND)
    public ResponseEntity<?> handleNotFountException(RuntimeException e) {

        LOGGER.info(e.getMessage());
        return ResponseEntity.status(NOT_FOUND).body(e.getMessage());
    }

    @ExceptionHandler({MethodArgumentNotValidException.class, BindException.class})
    @ResponseStatus(BAD_REQUEST)
    public ResponseEntity<?> handleBeanValidationException(MethodArgumentNotValidException e) {

        var errors = e.getBindingResult().getFieldErrors()
                .stream()
                .collect(toMap(
                        FieldError::getField,
                        DefaultMessageSourceResolvable::getDefaultMessage
                ));

        LOGGER.warn("Bean validation error: {}", errors);
        return ResponseEntity.status(BAD_REQUEST).body(errors);
    }

    @ExceptionHandler(GateNotInUseException.class)
    @ResponseStatus(BAD_REQUEST)
    public ResponseEntity<?> handleGateNotInUseException(GateNotInUseException e) {

        LOGGER.info(e.getMessage());
        return ResponseEntity.status(BAD_REQUEST).body(e.getMessage());
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(INTERNAL_SERVER_ERROR)
    public ResponseEntity<?> handleException(Exception e) {

        LOGGER.error("Unhandled exception: " + e.getMessage() + "\n", e);
        return ResponseEntity
                .status(INTERNAL_SERVER_ERROR)
                .body(e.getMessage());
    }
}
