package com.example.airportgatemanagementapi.controller;

import com.example.airportgatemanagementapi.controller.dto.UpdateAvailabilityDto;
import com.example.airportgatemanagementapi.service.GateService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/gate")
public class GateController {


    private final GateService gateService;

    @GetMapping
    public ResponseEntity<String> getAvailableGateNumber(@RequestParam String flightNumber) {
        return ResponseEntity.ok(gateService.getAvailableGateNumber(flightNumber));
    }

    @PatchMapping("/{gateNumber}")
    public ResponseEntity<?> releaseGate(@PathVariable String gateNumber) {

        gateService.releaseGate(gateNumber);
        return ResponseEntity.ok().build();
    }

    @PutMapping("/{gateNumber}")
    public ResponseEntity<?> updateGateAvailability(@RequestBody UpdateAvailabilityDto availabilityDto,
                                                    @PathVariable String gateNumber) {

        gateService.updateAvailability(gateNumber, availabilityDto.getAvailableFrom(), availabilityDto.getAvailableTo());
        return ResponseEntity.ok().build();
    }
}
