package com.example.airportgatemanagementapi.controller.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.sql.Time;

@Data
public class UpdateAvailabilityDto {

    @JsonFormat(pattern = "hh:mm:ss")
    private Time availableFrom;
    @JsonFormat(pattern = "hh:mm:ss")
    private Time availableTo;
}
