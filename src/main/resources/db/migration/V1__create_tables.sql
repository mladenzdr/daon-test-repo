create table if not exists flight
(
    id            UUID primary key,
    flight_number varchar unique
);

create table if not exists gate
(
    id             UUID primary key,
    gate_number    varchar unique,
    available_from time,
    available_to   time,
    flight_id      UUID,
    foreign key (flight_id) references flight (id)
)